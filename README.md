# UTU Trust SDK


UTU’s Trust API seamlessly serves up personalized recommendations for trusted service providers on sharing platforms to drive your conversion, satisfaction, retention, and viral acquisition.

[More information about UTU](https://utu.io)

UTU Trust SDK provides web-components that access UTU’s Trust API and allows you simple integration with its services.

Find the [repository here](https://bitbucket.org/utu-technologies/utu-trust-sdk)

## Features

- Integration with UTU's Trust API

## Examples

You can find other examples in the link below

[vue example](https://bitbucket.org/utu-technologies/utu-sdk-vue-example-app/src/master/)

[angular example](https://bitbucket.org/utu-technologies/utu-sdk-angular-example-app/src/master/)

[vanilla javascript example](https://bitbucket.org/utu-technologies/utu-sdk-vanilla-js-example-app/src/master/)





Before running examples, run ```npm install``` in the root repository folder,then run ```npm start```


### Quickstart React

Just import ```@ututrust/web-components``` and you are ready to start using UTU SKD.

```jsx
import '@ututrust/web-components';

function App() {

  const offerIds = [
    'e541df40-74b6-478e-a7da-7a9e52778700'
  ];

  return <div className="App">
      <x-utu-root api-key="<place your utu api key here>">
        <ul>
          {
            offerIds.map(offerId =>
              <li key={offerId}>
                <x-utu-recommendation target-uuid={offerId} />
              </li>
            )
          }
        </ul>
      </x-utu-root>
    </div>;
}

export default App;
```
